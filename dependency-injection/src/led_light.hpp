#ifndef LED_LIGHT_HPP
#define LED_LIGHT_HPP

#include <iostream>

class ILEDLight
{
public:
    virtual void set_rgb(int r, int g, int b) = 0;
    virtual ~ILEDLight() = default;
};

class LEDLight : public ILEDLight
{
public:
    void set_rgb(int r, int g, int b)
    {
        std::cout << "Setting(" << r << ", " << g << ", " << b << ")\n";
    }
};

#endif //LED_LIGHT_HPP

