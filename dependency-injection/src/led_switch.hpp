#ifndef LED_SWITCH_HPP
#define LED_SWITCH_HPP

#include "led_light.hpp"
#include "button.hpp"

class LEDSwitch : public InterfaceInjection::ISwitch
{
    ILEDLight& led_;

public:
    LEDSwitch(ILEDLight& led) : led_{led}
    {}

    void on() override
    {
        led_.set_rgb(255, 255, 255);
    }

    void off() override
    {
        led_.set_rgb(0, 0, 0);
    }
};

#endif