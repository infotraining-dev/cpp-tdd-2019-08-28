#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "led_light.hpp"
#include <memory>

namespace InterfaceInjection
{
    class ISwitch
    {
    public:
        virtual void on() = 0;
        virtual void off() = 0;
        virtual ~ISwitch() = default;
    };

    class Button
    {
        std::unique_ptr<ISwitch> light_;
        bool is_on_;

    public:
        Button(std::unique_ptr<ISwitch> light)
            : light_ {std::move(light)}
            , is_on_ {false}
        {
        }

        void click()
        {
            if (!is_on_)
            {
                light_->on();
                is_on_ = true;
            }
            else
            {
                light_->off();
                is_on_ = false;
            }
        }
    };
}

namespace TemplateParameterInjection
{
    class TSwitchParam;

    template <typename TSwitch = class TSwitchParam>
    class Button
    {
        TSwitch& light_switch_;
        bool is_on_;
    public:
        Button(TSwitch& light_switch) : light_switch_{light_switch}, is_on_{false}
        {}

        void click()
        {
            if (!is_on_)
            {
                light_switch_.on();
                is_on_ = true;
            }
            else
            {
                light_switch_.off();
                is_on_ = false;
            }
        }
    };
}

#endif