#include "button.hpp"
#include "led_light.hpp"
#include "led_switch.hpp"
#include <boost/di.hpp>
#include <iostream>

using namespace std;
namespace di = boost::di;

int main()
{
    // InterfaceInjection::Button btn(led_switch);

    const auto injector_low_perf = di::make_injector(
		di::bind<ILEDLight>().to<LEDLight>(),
        di::bind<InterfaceInjection::ISwitch>().to<LEDSwitch>());

    auto btn = injector_low_perf.create<InterfaceInjection::Button>();

    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();

    cout << "\n------\n";

    // LEDLight led;
    // LEDSwitch led_switch(led);
    // TemplateParameterInjection::Button btn2 {led_switch};
	auto injector_high_perf = di::make_injector(
		di::bind<class TemplateParameterInjection::TSwitchParam>().to<LEDSwitch>(),
		di::bind<ILEDLight>().to<LEDLight>()
	);

	auto btn2 = injector_high_perf.create<TemplateParameterInjection::Button>();

    btn2.click();
    btn2.click();
    btn2.click();
    btn2.click();
    btn2.click();
    btn2.click();

    return 0;
}