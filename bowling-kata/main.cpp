#include <iostream>
#include "bowling.hpp"

using namespace std;

int main()
{
    BowlingGame game;

    for(int i = 0; i < 12; ++i)
        game.roll(BowlingGame::max_pins_in_frame);

    cout << "Perfect game: " << game.score() << endl;
}