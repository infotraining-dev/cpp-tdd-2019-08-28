#ifndef BOWLING_HPP
#define BOWLING_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <tuple>

class BowlingGame
{
    static constexpr unsigned int max_rolls_in_game = 21;
    std::array<unsigned int, max_rolls_in_game> pins_ = {};
    unsigned int roll_no_{};

    auto score_for_frame(unsigned int roll_index) const
    {
        unsigned int result{};

        if (is_strike(roll_index))
        {
            result += max_pins_in_frame + strike_bonus(roll_index);
            ++roll_index;

            return std::tuple(result, roll_index);
        }
        else 
        {   
            if (is_spare(roll_index))
            {
                result += spare_bonus(roll_index);
            }

            result += frame_score(roll_index);

            roll_index += frame_size;
        }

        return std::tuple(result, roll_index);
    }

public:
    void roll(unsigned int pins)
    {
        pins_[roll_no_++] = pins;
    }

    unsigned int score() const
    {
        unsigned int result{};
        unsigned int roll_index{};

        for(unsigned int i = 0; i < frames_in_game; ++i)
        {
            auto [frame_score, next_roll_index] = score_for_frame(roll_index);
            
            result += frame_score;
            roll_index = next_roll_index;
        }

        return result;
    }

    static constexpr unsigned int max_pins_in_frame = 10;
private:    
    static constexpr unsigned int frame_size = 2;
    static constexpr unsigned int frames_in_game = 10;    

    bool is_spare(unsigned int roll_index) const
    {
        return frame_score(roll_index) == max_pins_in_frame;
    }

    unsigned int spare_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + frame_size];
    }

    unsigned int frame_score(unsigned int roll_index) const
    {
        return pins_[roll_index] + pins_[roll_index+1];
    }

    bool is_strike(unsigned int roll_index) const
    {
        return pins_[roll_index] == max_pins_in_frame;
    }

    unsigned int strike_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + 1] + pins_[roll_index + 2];
    }
};

#endif