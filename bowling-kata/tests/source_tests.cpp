#include "bowling.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace std;
using namespace ::testing;

struct BowlingGameTests : ::testing::Test
{
    BowlingGame game;

    void roll_many(unsigned int count, unsigned int pins)
    {
        for(unsigned int i = 0; i < count; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(1);
        game.roll(9); 
    }

    void roll_strike()
    {
        game.roll(BowlingGame::max_pins_in_frame);
    }
};

TEST_F(BowlingGameTests, for_new_game_score_is_zero)
{
    ASSERT_EQ(game.score(), 0);
}

TEST_F(BowlingGameTests, all_rolls_in_a_gutter_score_is_zero)
{
    roll_many(20, 0);

    ASSERT_EQ(game.score(), 0);
}

TEST_F(BowlingGameTests, when_all_rolls_no_mark_score_is_sum_of_pins)
{
    roll_many(20, 1);

    ASSERT_EQ(game.score(), 20);
}

TEST_F(BowlingGameTests, when_spare_next_roll_is_counted_twice)
{
    roll_spare();
    roll_many(18, 1);

    ASSERT_EQ(game.score(), 29);
}

TEST_F(BowlingGameTests, when_strike_two_next_rolls_are_counted_twice)
{
    roll_many(2, 1);
    roll_strike();
    roll_many(16, 1);

    ASSERT_EQ(game.score(), 30);
}

TEST_F(BowlingGameTests, when_spare_in_last_frame_one_extra_roll)
{
    roll_many(18, 1);
    roll_spare();
    game.roll(6);

    ASSERT_EQ(game.score(), 34);
}

TEST_F(BowlingGameTests, when_strike_in_last_frame_two_extra_rolls)
{
    roll_many(18, 1);
    roll_strike();
    game.roll(6);
    game.roll(2);

    ASSERT_THAT(game.score(), ::testing::Eq(36));
}

TEST_F(BowlingGameTests, perfect_game)
{
    roll_many(12, BowlingGame::max_pins_in_frame);        

    ASSERT_EQ(game.score(), 300);
}

// parametrized tests

struct BowlingTestParams
{
    std::initializer_list<unsigned int> rolls;
    unsigned int expected_score;
};

std::ostream& operator<<(std::ostream& out, const BowlingTestParams& params)
{
    out << "{ rolls: { ";
    for(const auto& pins : params.rolls)
        out << pins << " ";
    out << "}, score: "<< params.expected_score << " }";

    return out;

}

struct BowlingBulkTests : ::testing::TestWithParam<BowlingTestParams>
{};

TEST_P(BowlingBulkTests, game_score)
{
    BowlingGame game;

    auto params = GetParam();

    for(const auto& pins : params.rolls)
        game.roll(pins);
    
    ASSERT_EQ(game.score(), params.expected_score);
}

BowlingTestParams bowling_test_input[] = {
    { {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 20},
    { { 10, 3, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 44 },
    { { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5 }, 38 },
    { {1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 10 }, 119 },
    { { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}, 300 } 
};


INSTANTIATE_TEST_CASE_P(PackOfBowlingTests, 
    BowlingBulkTests, 
    ::testing::ValuesIn(bowling_test_input));