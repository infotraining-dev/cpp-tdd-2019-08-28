#include "source.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace std;
using namespace ::testing;

struct SimpleMock
{
    MOCK_METHOD(std::string, foo, (int));
};

TEST(SourceTests, Arrange_Act_Assert)
{
    SimpleMock mock;

    EXPECT_CALL(mock, foo(::testing::Gt(10))).WillOnce(Return("abc"));

    auto result = mock.foo(42);

    ASSERT_THAT(result, Eq("abc"));
}