#include "catch.hpp"
#include "source.hpp"

using namespace std;

TEST_CASE("adding item to vector", "[vector][push_back]")
{
    // Arrange
    vector<int> vec;
    REQUIRE(vec.size() == 0);

    // Act
    vec.push_back(4);

    SECTION("increases the size")
    {
        // Assert
        REQUIRE(vec.size() == 1);
    }

    SECTION("increases capacity")
    {
        // Assert
        REQUIRE(vec.capacity() >= 1);
    }

    REQUIRE_FALSE(vec.empty());
}

SCENARIO("adding item to vector")
{
    GIVEN("vector in default state")
    {
        vector<int> vec;

        WHEN("I insert an item")
        {
            vec.push_back(1);

            THEN("size is increased")   
            {
                REQUIRE(vec.size() == 1);
            }

            THEN("capacity is increased")
            {
                REQUIRE(vec.capacity() >= 1);
            }
        }
    }
}