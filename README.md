# Test-Driven Development in C++

## Docs

* https://infotraining.bitbucket.io/cpp-tdd
* https://gitpitch.com/infotraining-dev/cpp-tdd-slides/master?grs=bitbucket#/12

## PROXY

### Proxy settings ###

* add to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

## Ankieta

* https://docs.google.com/forms/d/e/1FAIpQLSdZwlxtAgzaCvxCwCLUROjk7C06ZmMrvTMmqdqVnX7OshSCGg/viewform?hl=pl